//
//  TutorDetail.swift
//  example_swift_ui
//
//  Created by Yu-An Hsiao on 2021/1/18.
//

import SwiftUI

struct TutorDetailView: View {
  
  var bio: String
  var name: String
  var headline: String
  
  var body: some View {
    VStack {
      Image(name)
        .clipShape(Circle())
        .overlay(Circle()
                  .stroke(Color.orange, lineWidth: 4.0))
        .shadow(radius: 10)
      Text(name)
        .font(.title)
      Text(headline)
        .font(.subheadline)
      Divider()
      Text(bio)
        .font(.headline)
        .multilineTextAlignment(.center)
        .lineLimit(.max)
    }
    .padding()
  }
}

struct TutorDetail_Previews: PreviewProvider {
  static var previews: some View {
    TutorDetailView(bio: tutors.first?.bio ?? "",
                name: tutors.first?.name ?? "",
                headline: tutors.first?.headline ?? "")
  }
}
