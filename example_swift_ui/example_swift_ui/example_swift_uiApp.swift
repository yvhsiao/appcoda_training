//
//  example_swift_uiApp.swift
//  example_swift_ui
//
//  Created by Yu-An Hsiao on 2021/1/18.
//

import SwiftUI

@main
struct example_swift_uiApp: App {
  var body: some Scene {
    WindowGroup {
      TutorsListView(tutors: tutors)
    }
  }
}
