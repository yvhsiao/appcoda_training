//
//  TutorsListView.swift
//  example_swift_ui
//
//  Created by Yu-An Hsiao on 2021/1/18.
//

import SwiftUI

struct TutorsListView: View {
  
  let tutors: [Tutor]
  let navigationTitle = "Tutors"
  
  var body: some View {
    NavigationView {
      List(tutors) { tutor in
        TutorCellView(tutor: tutor)
      }
      .navigationTitle(navigationTitle)
    }
  }
}

struct TutorCellView: View {
  
  let tutor: Tutor
  
  var body: some View {
    NavigationLink(
      destination: TutorDetailView(bio: tutor.bio,
                               name: tutor.name,
                               headline: tutor.headline)) {
      HStack {
        Image(tutor.name)
          .clipShape(Circle())
        VStack(alignment: .leading) {
          Text(tutor.name)
          Text(tutor.headline)
            .font(.subheadline)
            .foregroundColor(.gray)
        }
      }
    }
  }
}

#if DEBUG
struct TutorsListView_Previews: PreviewProvider {
  
  static var previews: some View {
    TutorsListView(tutors: tutors)
  }
}
#endif
